/*
***********************************************************************************
*			
*功能：
*
*
*
*说明：1.
*      2.
*      3.
*
*
*By		:Jiang
*Contact	:1247612904@qq.com
*History	:2014/9/7 13:49:11
***********************************************************************************
*/
#ifndef _COMMUNICATION_A_H_
#define _COMMUNICATION_A_H_

#include "lb_type.h"
#include "comm_rec.h"
#include "uart_idev.h"
#include "comm_reply.h"

#ifdef _COMMUNICATION_A_C_
#define COMMUNICATION_A_EXT
#else 
#define COMMUNICATION_A_EXT extern
#endif 

#define MODBUS_INPUT_SFR	0x04 


typedef struct{
	INT8U  enable;
	INT8U  status;
	INT16U startTime;
	INT16U likelyEndTime;

}Light_State ;

typedef union{
	INT8U buf[18];
	struct{
		Light_State red;
		Light_State yellow;
		Light_State green;
	}dat;
}comm_union_dat;

#ifdef _COMMUNICATION_A_C_
comm_union_dat cmm_dat;
#endif

#define COMM_TIM_UP	0x01
#define COMM_TIM_WAIT 0x00 

typedef struct
{
    INT8U status;
	INT8U addr;
	comm_rec_struct *cmm_uart_rec;
}communication_struct;

COMMUNICATION_A_EXT communication_struct communication_data;

COMMUNICATION_A_EXT void communication_a_init(void);
COMMUNICATION_A_EXT void communication_a_tick(void);
COMMUNICATION_A_EXT void communication_a_exc(void);

COMMUNICATION_A_EXT void load_comm_vol(INT16U vol);
COMMUNICATION_A_EXT void load_comm_addr(INT8U addr);

COMMUNICATION_A_EXT void load_comm_light(INT16U type,	INT8U  status,	INT16U startTime,	INT16U likelyEndTime);

#endif