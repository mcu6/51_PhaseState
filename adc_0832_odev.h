/*
***********************************************************************************
*			
*功能：
*
*
*
*说明：1.
*      2.
*      3.
*
*
*By		:Jiang
*Contact	:1247612904@qq.com
*History	:2014/8/5 22:11:29
***********************************************************************************
*/
#ifndef _ADC_0832_ODEV_H_
#define _ADC_0832_ODEV_H_

#include "lb_type.h"
#include "adc_0832_odev_io.h"

#ifdef _ADC_0832_ODEV_C_
#define ADC_0832_ODEV_EXT
#else 
#define ADC_0832_ODEV_EXT extern
#endif 

#ifdef _ADC_0832_ODEV_C_
#endif


typedef struct
{
    INT8U status;
}adc_0832_struct;

ADC_0832_ODEV_EXT adc_0832_struct adc_0832_data;

ADC_0832_ODEV_EXT void adc_0832_odev_init(void);
ADC_0832_ODEV_EXT INT8U get_ad_result();
#endif