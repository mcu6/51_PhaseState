#define _COMMUNICATION_C_
#include "communication.h"

/*模块结构体初始化*/
void communication_struct_init(void)
{
	cmm_dat.dat.vol = 0;
	communication_data.addr = 1;
}

INT16U cmb_modbus_2byte(INT8U *buf,INT8U start)
{
	return ((((INT16U)buf[start])<<8)+buf[start+1]);
}



void comm_ok(void* msg)
{
	static INT8U sbuf[8];
	INT8U i,func,index = 0,s_num;
	INT16U sfr_start_addr,sfr_num;
	comm_rec_struct *p_data;
	p_data = (comm_rec_struct *)msg;
	func = p_data->buf[1];
	if(func == MODBUS_INPUT_SFR)
	{
		sfr_start_addr = cmb_modbus_2byte(p_data->buf,2);
		if(sfr_start_addr == 0x0000)
		{
			sfr_num = cmb_modbus_2byte(p_data->buf,4);
			if(sfr_num == 0x0001)
			{
				s_num = (INT8U)(sfr_num<<1);
				sbuf[index++] = p_data->buf[0];
				sbuf[index++] = p_data->buf[1];
				sbuf[index++] = 0x02;
				for(i = 0;i < s_num;i ++)
				{
					sbuf[index++] = cmm_dat.buf[i];
				}
				exc_comm_reply(sbuf,index);
					
			}		
		}		
	}

	
}

/*模块初始化*/
void communication_init(void)
{

  communication_struct_init();
  communication_data.cmm_uart_rec = comm_rec_create(communication_data.addr,
										10,
					   					(FUNC_COMM_REC *)uart_rec,
										(FUNC_COMM_REC_OK *)comm_ok,
										(FUNC_COMM_REC_OK *)0);

}

void load_comm_vol(INT16U vol)
{
	cmm_dat.dat.vol = vol;
}

void load_comm_addr(INT8U addr)
{
	communication_data.addr = addr;
	comm_rec_set_addr(communication_data.cmm_uart_rec,communication_data.addr);
}

