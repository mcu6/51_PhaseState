/*
***********************************************************************************
*			
*功能：
*
*
*
*说明：1.
*      2.
*      3.
*
*
*By		:Jiang
*Contact	:1247612904@qq.com
*History	:2014/8/31 13:47:13
***********************************************************************************
*/
#ifndef _UART_IDEV_H_
#define _UART_IDEV_H_

#include "lb_type.h"

#ifdef _UART_IDEV_C_
#define UART_IDEV_EXT
#else 
#define UART_IDEV_EXT extern
#endif 

#ifdef _UART_IDEV_C_
#endif 
#define FOSC    22118400L    //11059200L  //44236800L  //44236800L   // ---44.236800	// 11059200L		
#define BAUD    57600.0       //57600.0 //9600.0    //19200.0//57600.0 //4800.0   //9600.0
 
typedef struct
{
    INT8U status;
}uart_struct;

UART_IDEV_EXT uart_struct uart_data;

UART_IDEV_EXT void uart_idev_init(void);
UART_IDEV_EXT void uart_send_byte(BYTE dat);
UART_IDEV_EXT INT8U uart_rec(void);
#endif