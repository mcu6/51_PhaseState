
#ifndef _CRC16_H_
#define _CRC16_H_

	#ifndef _CRC16_C_
	#define CRC16_EXT extern
	#else 
	#define CRC16_EXT
	#endif
	
	CRC16_EXT unsigned int CRC16_2(unsigned char *pchMsg, unsigned int wDataLen);  //CRC16 校验 函数声明

#endif