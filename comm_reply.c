#define _COMM_REPLY_C_
#include "comm_reply.h"

/*模块结构体初始化*/
void comm_reply_struct_init(void)
{
	
}

void exc_comm_reply(INT8U *buf,INT8U num)
{
	INT8U i;
	for(i = 0;i < num;i ++)
	{
		comm_reply_data.f_send_byte(buf[i]);	
	}
}

/*模块初始化*/
void comm_reply_init(f_comm_send_byte *f_send_byte)
{
   comm_reply_struct_init();
   comm_reply_data.f_send_byte = f_send_byte;
}

