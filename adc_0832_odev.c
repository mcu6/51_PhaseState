#define _ADC_0832_ODEV_C_
#include "adc_0832_odev.h"
#include "intrins.h"

/*模块结构体初始化*/
void adc_0832_odev_struct_init(void)
{

}

/*模块初始化*/
void adc_0832_odev_init(void)
{

   adc_0832_odev_struct_init();


}

//采集并返回
INT8U get_ad_result()
{
 	INT8U i=0;
	INT8U j;
	INT16U dat=0;
	INT8U ndat=0;

	ADDI=1;
	_nop_();
	_nop_();
	ADCS0=0;//拉低CS端
	_nop_();
	_nop_();
	ADCLK=1;//拉高CLK端
	_nop_();
	_nop_();
	ADCLK=0;//拉低CLK端,形成下降沿1
	_nop_();
	_nop_();
	ADCLK=1;//拉高CLK端
	ADDI = 0;
	//ADDI=channel&0x1;
	_nop_();
	_nop_();
	ADCLK=0;//拉低CLK端,形成下降沿2
	_nop_();
	_nop_();
	ADCLK=1;//拉高CLK端
	ADDI = 1;
	//ADDI=(channel>>1)&0x1;
	_nop_();
	_nop_();
	ADCLK=0;//拉低CLK端,形成下降沿3
	ADDI=1;//控制命令结束 
	_nop_();
	_nop_();
	dat=0;
	for(i=0;i<8;i++)
	{
		dat|=ADDO;//收数据
		ADCLK=1;
		_nop_();
		_nop_();
		ADCLK=0;//形成一次时钟脉冲
		_nop_();
		_nop_();
		dat<<=1;
		if(i==7)dat|=ADDO;
	}  
	for(i=0;i<8;i++)
	{
		j=0;
		j=j|ADDO;//收数据
		ADCLK=1;
		_nop_();
		_nop_();
		ADCLK=0;//形成一次时钟脉冲
		_nop_();
		_nop_();
		j=j<<7;
		ndat=ndat|j;
		if(i<7)ndat>>=1;
	}
	ADCS0=1;//拉低CS端
	ADCLK=0;//拉低CLK端
	ADDO=1;//拉高数据端,回到初始状态
	dat<<=8;
	dat|=ndat;
 	return(uchar)(dat);            //return ad data
}

