/*
***********************************************************************************
*			
*功能：
*
*
*
*说明：1.
*      2.
*      3.
*
*
*By		:Jiang
*Contact	:1247612904@qq.com
*History	:2014/9/7 10:16:20
***********************************************************************************
*/
#ifndef _COMMUNICATION_H_
#define _COMMUNICATION_H_

#include "lb_type.h"
#include "comm_rec.h"
#include "uart_idev.h"
#include "comm_reply.h"

#ifdef _COMMUNICATION_C_
#define COMMUNICATION_EXT
#else 
#define COMMUNICATION_EXT extern
#endif 

#define MODBUS_INPUT_SFR	0x04 
typedef union{
	INT8U buf[2];
	struct{
		INT16U vol;
	}dat;
}comm_union_dat;

#ifdef _COMMUNICATION_C_
comm_union_dat cmm_dat;
#endif



typedef struct
{
    INT8U status;
	INT8U addr;
	comm_rec_struct *cmm_uart_rec;
}communication_struct;

COMMUNICATION_EXT communication_struct communication_data;

COMMUNICATION_EXT void communication_init(void);
COMMUNICATION_EXT void load_comm_vol(INT16U vol);
COMMUNICATION_EXT void load_comm_addr(INT8U addr);
#endif