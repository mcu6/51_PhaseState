#define _TEXT_C_
#include "text.h"

/*模块结构体初始化*/
void text_struct_init(void)
{
	INT8U i;
	for(i = 0;i < TEXT_BUF_NUM;i ++)
	{
		text_data.buf[i] = '0';
	}
	text_data.buf[TEXT_BUF_NUM-1] = '\0';

	text_data.addr = 0;
	text_data.index = 0;
}

void txt_key_tick(INT8U key)
{
	INT8U i;
	INT16U tmp;
	key = key;
	if(text_data.index < 3)
	{
		if((key >= '0')&&(key <= '9'))
		{
			for(i = 0;i < 2;i ++)
			{
				text_data.buf[i] = text_data.buf[i+1];			
			}
			text_data.buf[i] = key;
			text_data.index ++;
		}
		text_data.buf[TEXT_BUF_NUM-1] = '\0';
		
	}
	if(key == 'c')
	{
	   for(i = 0;i < TEXT_BUF_NUM;i ++)
		{
			text_data.buf[i] = '0';
		}
		text_data.buf[TEXT_BUF_NUM-1] = '\0';
		text_data.index = 0;
	}
	if(key == '=')
	{
		tmp = (text_data.buf[0]-'0')*100+(text_data.buf[1]-'0')*10+(text_data.buf[2]-'0');
		if(tmp < 248)
		{
			text_data.addr = (INT8U)tmp;	
		}
	}
	//lcd_1602_wr_float(1,6,(float)key);
}

/*模块初始化*/
void text_init(void)
{

   text_struct_init();


}

char *get_text(void)
{
	return text_data.buf;
}

INT8U get_text_addr(void)
{
	return text_data.addr;
}

