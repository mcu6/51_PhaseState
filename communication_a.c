#define _COMMUNICATION_A_C_
#include "communication_a.h"
#include "crc16.h"

/*模块结构体初始化*/
void communication_a_struct_init(void)
{
//	cmm_dat.dat.vol = 0;
	communication_data.addr = 1;
	communication_data.status = COMM_TIM_WAIT;
}

INT16U cmb_modbus_2byte(INT8U *buf,INT8U start)
{
	return ((((INT16U)buf[start])<<8)+buf[start+1]);
}


void comm_ok(void* msg)
{
   union{    
      INT8U  crc[2];
      INT16U crc16;
    }CRC_check;
	
	static INT8U sbuf[8];
	INT8U i,func,index = 0,s_num;
	INT16U sfr_start_addr,sfr_num;
	comm_rec_struct *p_data;
	p_data = (comm_rec_struct *)msg;
	func = p_data->buf[1];
    
	// 01 04 0000 0009
	if(func == MODBUS_INPUT_SFR)
	{
		sfr_start_addr = cmb_modbus_2byte(p_data->buf,2);
		if(sfr_start_addr == 0x0000)
		{
			sfr_num = cmb_modbus_2byte(p_data->buf,4); //  要读取的字节数
			if(sfr_num == 0x0009)
			{
				s_num = (INT8U)(sfr_num<<1);
				sbuf[index++] = p_data->buf[0];  // addr
				sbuf[index++] = p_data->buf[1];  // func
				sbuf[index++] = 18;           // bytes
				for(i = 0;i < s_num;i ++)
				{
					sbuf[index++] = cmm_dat.buf[i];
				}

				CRC_check.crc16 = CRC16_2(sbuf,index);
				sbuf[index++] = CRC_check.crc[0];/* CRC校验码计算 */                  
				sbuf[index++] = CRC_check.crc[1];/* CRC校验码计算 */

				exc_comm_reply(sbuf,index);
			}		
		}		
	}

	
}

/*模块初始化*/
void communication_a_init(void)
{

	communication_a_struct_init();
	communication_data.cmm_uart_rec = comm_rec_create(communication_data.addr,
									30,
				   					(FUNC_COMM_REC *)uart_rec,
									(FUNC_COMM_REC_OK *)comm_ok,
									(FUNC_COMM_REC_OK *)0);

}

/*模块定时器中断函数*/
void communication_a_tick(void)
{
	communication_data.status = COMM_TIM_UP;	
}

/*模块执行处理主函数*/
void communication_a_exc(void)
{
    if(communication_data.status == COMM_TIM_UP)
	{
		communication_data.status = COMM_TIM_WAIT;
		comm_rec_set_addr(communication_data.cmm_uart_rec,communication_data.addr);
	}
}




void load_comm_light(INT16U type,	INT8U  status,	INT16U startTime,	INT16U likelyEndTime)
{
	if (type == 0){
	 cmm_dat.dat.red.status = status;
	 cmm_dat.dat.red.startTime = startTime;
	 cmm_dat.dat.red.likelyEndTime = likelyEndTime;
	}
	if (type ==1){
	 cmm_dat.dat.yellow.status = status;
	 cmm_dat.dat.yellow.startTime = startTime;
	 cmm_dat.dat.yellow.likelyEndTime = likelyEndTime;
	}
	if (type ==2){
	 cmm_dat.dat.green.status = status;
	 cmm_dat.dat.green.startTime = startTime;
	 cmm_dat.dat.green.likelyEndTime = likelyEndTime;
	}
}

void load_comm_vol(INT16U vol)
{
	//cmm_dat.dat.vol = vol;
}

void load_comm_addr(INT8U addr)
{
	communication_data.addr = addr;
}

