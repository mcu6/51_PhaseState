#define _LCD_1602_ODEV_C_
#include "lcd_1602_odev.h"

//*******延时函数*********
void lcd_1602_dly(uint postpone)
{
	uint  tmp;
	tmp = (postpone<<3);
	while(tmp--);
}

//********写指令函数************
void lcd_1602_wr_cmd(uchar LCD_COM)
{
	RS_1602=0;
	DataPort=LCD_COM;
	EN_1602=1;
	lcd_1602_dly(100);
	EN_1602=0;
}

//********写数据函数*************
void lcd_1602_wr_dat(INT8U dat)
{
	RS_1602=1;
	DataPort=dat;
	EN_1602=1;
	lcd_1602_dly(100);
	EN_1602=0;
}


void lcd_1602_disp_char(INT8U rows,INT8U column,INT8U chr)	  //LCD指定位置写数据
{
	INT8U addr = column;
	if(rows == 0)
		addr += 0x80;
	else addr += 0xc0;

	lcd_1602_wr_cmd(addr);
	lcd_1602_wr_dat(chr);
}

//********写液晶语句函数*************
//变量：y:行（取值0-1）x:列（取值0-15）
void lcd_1602_wr_string(INT8U rows,INT8U column,char *str)
{
	uchar addr = column;
	if(rows == 0)
		addr += 0x80;
	else addr += 0xc0;
	lcd_1602_wr_cmd(addr);
	while(*str)
	{
		lcd_1602_wr_dat(*str++);
	}
}

INT8U lcd_1602_wr_interger(INT8U rows,INT8U column,INT16U num)
{
	INT8U addr = column,i  = 0,index,buf[5];
	if(rows == 0)
	{
		addr +=0x80;
	}
	else 
	{
		addr += 0xc0;
	}
	if(num != 0)
	{
		while(num)
		{
			buf[i] = num%10;
			num /= 10;
			i ++;	
		}
	}
	else
	{
		buf[0] = 0;
		i = 1;
	}
	lcd_1602_wr_cmd(addr);
	index = i;
	while(i --)
	{
	   lcd_1602_wr_dat(buf[i]+48);
	}
	return index;
}

void lcd_1602_wr_float(INT8U rows,INT8U column,float val)
{
	INT16U num;
	INT8U index;
	num = (INT16U)val;
	index = lcd_1602_wr_interger(rows,column,num);
	lcd_1602_disp_char(rows,column+index,'.');
	num = 	(val - num)*100;
	if(num < 10)
	{
	   lcd_1602_wr_interger(rows,column+index+1,0);
	   lcd_1602_wr_interger(rows,column+index+2,num);
	}
	else
	{
		index = lcd_1602_wr_interger(rows,column+index+1,num);
	}
}
//***************************************************************

/*模块结构体初始化*/
void lcd_1602_odev_struct_init(void)
{

}

/*模块初始化*/
void lcd_1602_odev_init(void)
{
	lcd_1602_odev_struct_init();
	RW_1602=0;			  //初始化读写端口
	EN_1602=0;			  //初始化使能端口
	lcd_1602_wr_cmd(0x38);	  //设置8位格式，2行，5x7
	lcd_1602_wr_cmd(0x0c);	  //整体显示，关光标，不闪烁
	lcd_1602_wr_cmd(0x06);	  //设定输入方式，增量不移位
	lcd_1602_wr_cmd(0X01);	  //清除屏幕显示

}


typedef struct{
	INT8U rows;
	INT8U column;
	float val;
}LCD_FLOAT_MSG;

void lcd_show_f(void *msg)
{
	LCD_FLOAT_MSG *lcd_msg;
	lcd_msg = (LCD_FLOAT_MSG *)msg;
	 
	lcd_1602_wr_float(lcd_msg->rows,lcd_msg->column,lcd_msg->val);
}

typedef struct{
	INT8U rows;
	INT8U column;
	char *txt;
}LCD_TXT_MSG;

void lcd_show_s(void *msg)
{
	LCD_TXT_MSG *lcd_msg;
	lcd_msg = (LCD_TXT_MSG *)msg;

	lcd_1602_wr_string(lcd_msg->rows,lcd_msg->column,(char *)lcd_msg->txt);
}
