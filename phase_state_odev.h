/*
***********************************************************************************
*			
*功能：
*
*
*
*说明：1.
*      2.
*      3.
*
*
*By			:Jiang
*Contact	:1247612904@qq.com
*History	:2022/5/19 10:15:02
***********************************************************************************
*/
#ifndef _PHASE_STATE_ODEV_H_
#define _PHASE_STATE_ODEV_H_

#include "lb_type.h"
#include "phase_state_odev_io.h"

#ifdef _PHASE_STATE_ODEV_C_
#define PHASE_STATE_ODEV_EXT
#else 
#define PHASE_STATE_ODEV_EXT extern
#endif 

#ifdef _PHASE_STATE_ODEV_C_
#endif 


typedef struct{
	INT8U  enable;
	INT16U light_cnt_set;
	INT16U light_cnt_surplus;
	INT16U startTime;
	INT16U likelyEndTime;

}LightState ;

typedef struct
{
    INT8U status;

	LightState red_light_state;
	LightState yellow_light_state;
	LightState green_light_state;
	
	
}phase_state_struct;

PHASE_STATE_ODEV_EXT phase_state_struct phase_state_data;

PHASE_STATE_ODEV_EXT void phase_state_odev_init(void);

PHASE_STATE_ODEV_EXT phase_state_struct * phase_state_get_data(void);
#endif