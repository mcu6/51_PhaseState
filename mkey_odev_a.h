/*
***********************************************************************************
*			
*功能：
*
*
*
*说明：1.
*      2.
*      3.
*
*
*By		:Jiang
*Contact	:1247612904@qq.com
*History	:2014/8/30 19:10:17
***********************************************************************************
*/
#ifndef _MKEY_ODEV_A_H_
#define _MKEY_ODEV_A_H_

#include "lb_type.h"
#include "mkey_odev_a_io.h"

#ifdef _MKEY_ODEV_A_C_
#define MKEY_ODEV_A_EXT
#else 
#define MKEY_ODEV_A_EXT extern
#endif 

typedef union{
	struct{
		INT8U bit0:1;
		INT8U bit1:1;
		INT8U bit2:1;
		INT8U bit3:1;
		INT8U bit4:1;
		INT8U bit5:1;
		INT8U bit6:1;
		INT8U bit7:1;
	}bits;
	INT8U port;
}port_union;

#define MKEY_BLANK	0xff

#ifdef _MKEY_ODEV_A_C_
port_union key;
const key_trans[17]={MKEY_BLANK,'7','8','9','/','4','5','6','x','1','2','3','-','c','0','=','+'};
#endif

typedef void *F_MKEY_MSG(INT8U key);

#define MKEY_STATUS_TIM_WAIT	0x00
#define MKEY_STATUS_TIM_UP		0x01 

typedef struct
{
    INT8U status;
	INT8U key;
	F_MKEY_MSG	*f_mkey;
}mkey_struct;

MKEY_ODEV_A_EXT mkey_struct mkey_data;

MKEY_ODEV_A_EXT void mkey_odev_a_init(F_MKEY_MSG	*f_mkey);
MKEY_ODEV_A_EXT void mkey_odev_a_tick(void);
MKEY_ODEV_A_EXT void mkey_odev_a_exc(void);
MKEY_ODEV_A_EXT void attach_key(F_MKEY_MSG	*f_mkey);

MKEY_ODEV_A_EXT INT8U key_scan(void);

#endif