#ifndef _ADC_0832_ODEV_IO_H_
#define _ADC_0832_ODEV_IO_H_

#include "reg52.h"

//ADC0832的引脚
sbit ADCLK =P3^4;  	//ADC0832 clock signal
sbit ADDI =P3^6;  	//ADC0832 k in
sbit ADDO=P3^6;  	//ADC0832 k out
sbit ADCS0 =P3^7;  	//ADC0832 chip seclect

#endif