/*
***********************************************************************************
*			
*功能：
*
*
*
*说明：1.
*      2.
*      3.
*
*
*By		:Jiang
*Contact	:1247612904@qq.com
*History	:2014/9/7 12:41:18
***********************************************************************************
*/
#ifndef _TEXT_H_
#define _TEXT_H_

#include "lb_type.h"

#ifdef _TEXT_C_
#define TEXT_EXT
#else 
#define TEXT_EXT extern
#endif 

#ifdef _TEXT_C_
#endif 

#define TEXT_BUF_NUM	4

typedef struct
{
    INT8U status;
	char buf[TEXT_BUF_NUM];
	INT8U index;
	INT8U addr;
}text_struct;

TEXT_EXT text_struct text_data;
TEXT_EXT void txt_key_tick(INT8U key);

TEXT_EXT void text_init(void);
TEXT_EXT char *get_text(void);
TEXT_EXT INT8U get_text_addr(void);
#endif