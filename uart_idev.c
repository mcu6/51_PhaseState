#define _UART_IDEV_C_
#include "all.h"

/*模块结构体初始化*/
void uart_idev_struct_init(void)
{

}

/*模块初始化*/
void uart_idev_init(void)
{
	uart_idev_struct_init();
	SCON = 0x5a;                    //8 bit data ,no parity bit
	REN = 1;  //允许接收
	ES = 1;	  //串口中断打开
	RI = 0;	  //接收中断标志位清零
	TI = 1;
	
	TMOD |= 0x20;                    //T1 as 8-bit auto reload
	TH1 = TL1 = -(uchar)(FOSC/12.0/32.0/BAUD); //Set Uart baudrate
	TR1 = 1;                        //T1 start running

}

void uart_send_byte(BYTE dat)
{
    while (!TI);                    //Wait for the previous data is sent
    TI = 0;                         //Clear TI flag
    SBUF = dat;                     //Send current data
}

INT8U uart_rec(void)
{
	return SBUF;
} 

void uart(void) interrupt 4
{
	if(RI==1)
	{
		RI = 0;	
		comm_rec_handle(communication_data.cmm_uart_rec);
	}		
}

