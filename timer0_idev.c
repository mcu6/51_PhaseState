#define _TIMER0_IDEV_C_
#include "all.h"

/*模块结构体初始化*/
void timer0_idev_struct_init(void)
{

}

/*模块初始化*/
void timer0_idev_init(void)
{

	timer0_idev_struct_init();
	TMOD|=0X01;	//模式选择
	TH0=0xdc;  	//赋初值
	TL0=0x00; 
	ET0=1;	  	//开定时器1中断
	TR0=1;		//开计数器1中断
}

//5ms timer when TH0=0xb8;TL0=0x00; fosc = 44.2368MHz
//5ms timer when TH0=0xee;TL0=0x00; fosc = 11.0592MHz
//1ms timer when TH0=0xfc;TL0=0x66; fosc = 11.0592MHz

//5ms timer when TH0=0xdc;TL0=0x00; fosc = 22.1184MHz
//2ms timer when TH0=0xf1;TL0=0x99; fosc = 22.1184MHz
//1ms timer when TH0=0xf8;TL0=0xcc; fosc = 22.1184MHz
void t0(void) interrupt 1 //using 0 
{	
	TH0=0xdc;  	//赋初值
	TL0=0x00;
	LB_TMRTick(); 
}

