#define _PHASE_STATE_ODEV_C_
#include "phase_state_odev.h"

/*模块结构体初始化*/
void phase_state_odev_struct_init(void)
{
	phase_state_data.status = 0;
	phase_state_data.red_light_state.enable = TRUE;
	phase_state_data.red_light_state.light_cnt_set = 3 * 100;
	phase_state_data.red_light_state.light_cnt_surplus = 3 * 100;
	phase_state_data.red_light_state.startTime = 0;
	phase_state_data.red_light_state.likelyEndTime = 3 * 100;
	  
	phase_state_data.yellow_light_state.enable = FALSE;
	phase_state_data.yellow_light_state.light_cnt_set     = 3 * 100;
	phase_state_data.yellow_light_state.light_cnt_surplus = 3 * 100;
	phase_state_data.yellow_light_state.startTime         = 3 * 100;
	phase_state_data.yellow_light_state.likelyEndTime     = 3 * 100;

	phase_state_data.green_light_state.enable            = FALSE;
	phase_state_data.green_light_state.light_cnt_set     = 3*100;
	phase_state_data.green_light_state.light_cnt_surplus = 3*100;
	phase_state_data.green_light_state.startTime         = 3*100;
	phase_state_data.green_light_state.likelyEndTime     = 3*100;
	  
}

/*模块初始化*/
void phase_state_odev_init(void)
{

   phase_state_odev_struct_init();
	
	 LIGHT_RED = TRUE;
	 LIGHT_YELLOW =TRUE;
	 LIGHT_GREEN =TRUE;
}


phase_state_struct * phase_state_get_data(void)
{
  return &phase_state_data;
}




