#ifndef _LCD_1602_ODEV_IO_H_
#define _LCD_1602_ODEV_IO_H_
#include "reg52.h"

#define DataPort P0 
sbit RS_1602=P1^1;
sbit RW_1602=P1^2;
sbit EN_1602=P1^3;

#endif