#ifndef _MKEY_ODEV_A_IO_H_
#define _MKEY_ODEV_A_IO_H_
#include "reg52.h"

sbit key_bit0 = P2^0;
sbit key_bit1 = P2^1;
sbit key_bit2 = P2^2;
sbit key_bit3 = P2^3;
sbit key_bit4 = P2^4;
sbit key_bit5 = P2^5;
sbit key_bit6 = P2^6;
sbit key_bit7 = P2^7;

#endif