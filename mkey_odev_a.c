#define _MKEY_ODEV_A_C_
#include "mkey_odev_a.h"

/*模块结构体初始化*/
void mkey_odev_a_struct_init(void)
{
	mkey_data.status = MKEY_STATUS_TIM_WAIT;
	mkey_data.key = 0;
}

/*模块初始化*/
void mkey_odev_a_init(F_MKEY_MSG	*f_mkey)
{

   mkey_odev_a_struct_init();
   mkey_data.f_mkey = f_mkey;

}

/*模块定时器中断函数*/
void mkey_odev_a_tick(void)
{
	mkey_data.status = MKEY_STATUS_TIM_UP;	
}

void load_key_port(INT8U val)
{
	key.port = val;
	key_bit0 = key.bits.bit0;
	key_bit1 = key.bits.bit1;
	key_bit2 = key.bits.bit2;
	key_bit3 = key.bits.bit3;
	key_bit4 = key.bits.bit4;
	key_bit5 = key.bits.bit5;
	key_bit6 = key.bits.bit6;
	key_bit7 = key.bits.bit7;
		
}

INT8U read_key_port(void)
{
	key.bits.bit0 = key_bit0;
	key.bits.bit1 = key_bit1;
	key.bits.bit2 = key_bit2;
	key.bits.bit3 = key_bit3;
	key.bits.bit4 = key_bit4;
	key.bits.bit5 = key_bit5;
	key.bits.bit6 = key_bit6;
	key.bits.bit7 = key_bit7;
	return key.port;	
}

INT8U key_scan(void)
{
	const uchar scan[4] = {0xef,0xdf,0xbf,0x7f};
	uchar KeyValue=0x00,i,j,k;
	load_key_port(0X0F);
	if(read_key_port() != 0X0F)
	{
		for(i = 0;i < 4;i ++)
		{
			load_key_port(scan[i]);
			for(j = 0;j < 4;j ++)
			{
				k = read_key_port()&0X0F;
				if(k == (scan[j]>>4))
				{
					KeyValue = j*4+i+1;
					break;	
				}	
			}
		}
	}
	return KeyValue;	
}

/*模块执行处理主函数*/
void mkey_odev_a_exc(void)
{
	if(mkey_data.status == MKEY_STATUS_TIM_UP)
	{
		mkey_data.status = MKEY_STATUS_TIM_WAIT;
		mkey_data.key = key_scan();
		if(mkey_data.key > 0)
		{
		   mkey_data.f_mkey(key_trans[mkey_data.key]);
		}
	}		
}

void attach_key(F_MKEY_MSG	*f_mkey)
{
	mkey_data.f_mkey = f_mkey;
}

