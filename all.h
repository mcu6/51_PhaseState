/*
***********************************************************************************
*			
*功能：
*
*
*
*说明：1.
*      2.
*      3.
*
*
*By		:Jiang
*Contact	:1247612904@qq.com
*History	:2014/8/31 13:33:54
***********************************************************************************
*/
#ifndef _ALL_H_
#define _ALL_H_

#include "lb_type.h"
#include "reg52.h"
#include "main.h"
#include "timer0_idev.h"
#include "timer_core.h"
#include "uart_idev.h"
#include "comm_rec.h"
#include "communication_a.h"
#include "comm_reply.h"
#include "lcd_1602_odev.h"
#include "lcd_show_a.h"
#include "mkey_odev_a.h"
#include "text.h"
#include "adc_0832_odev.h"
#include "vol_smp_a.h"
#include "phase_state_odev.h"

#ifdef _ALL_C_
#define ALL_EXT
#else 
#define ALL_EXT extern
#endif 

#ifdef _ALL_C_
#endif 


#endif