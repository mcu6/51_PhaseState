#define _VOL_SMP_A_C_
#include "vol_smp_a.h"

/*模块结构体初始化*/
void vol_smp_a_struct_init(void)
{
	vol_smp_data.status	= VOL_SMP_STATUS_TIM_WAIT;
}

/*模块初始化*/
void vol_smp_a_init(FUNC_GET_AD_RESULT *f_get_ad_result)
{

   vol_smp_a_struct_init();
   vol_smp_data.f_get_ad_result = f_get_ad_result;

}

float ad_to_vol(INT8U ad_val)
{
	return (5.0*ad_val/255);
}

/*模块定时器中断函数*/
void vol_smp_a_tick(void)
{
	vol_smp_data.status	= VOL_SMP_STATUS_TIM_UP;
}

/*模块执行处理主函数*/
void vol_smp_a_exc(void)
{
	if(vol_smp_data.status	== VOL_SMP_STATUS_TIM_UP)
	{
		vol_smp_data.status	= VOL_SMP_STATUS_TIM_WAIT;
		vol_smp_data.voltage = ad_to_vol(vol_smp_data.f_get_ad_result());
	}
}

float get_vol_smp(void)
{
	return vol_smp_data.voltage;
}

