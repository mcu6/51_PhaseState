/*
***********************************************************************************
*			
*功能：
*
*
*
*说明：1.
*      2.
*      3.
*
*
*By		:Jiang
*Contact	:1247612904@qq.com
*History	:2014/8/31 13:32:27
***********************************************************************************
*/
#ifndef _MAIN_H_
#define _MAIN_H_

#include "lb_type.h"
#include "timer_core.h"
#include "comm_rec.h"

#ifdef _MAIN_C_
#define MAIN_EXT
#else 
#define MAIN_EXT extern
#endif 

#ifdef _MAIN_C_
#endif 

typedef struct
{
    INT8U status;
	TIMER_STRUCT *tmr_mtick;
}main_struct;

MAIN_EXT main_struct main_data;
#endif