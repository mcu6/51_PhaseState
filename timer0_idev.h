/*
***********************************************************************************
*			
*功能：
*
*
*
*说明：1.
*      2.
*      3.
*
*
*By		:Jiang
*Contact	:1247612904@qq.com
*History	:2014/8/31 13:36:56
***********************************************************************************
*/
#ifndef _TIMER0_IDEV_H_
#define _TIMER0_IDEV_H_

#include "lb_type.h"

#ifdef _TIMER0_IDEV_C_
#define TIMER0_IDEV_EXT
#else 
#define TIMER0_IDEV_EXT extern
#endif 

#ifdef _TIMER0_IDEV_C_
#endif 

typedef struct
{
    INT8U status;
}timer0_struct;

TIMER0_IDEV_EXT timer0_struct timer0_data;

TIMER0_IDEV_EXT void timer0_idev_init(void);
#endif