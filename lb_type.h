#ifndef _LB_TYPE_H_
#define _LB_TYPE_H_

typedef unsigned char  	INT8U;         //无符号8位数
typedef signed   char  	INT8S;         //有符号8位数
typedef unsigned short  INT16U;        //无符号16位数
typedef signed   short  INT16S;        //有符号16位数

typedef INT8U		 	BYTE;
typedef INT16U		 	WORD;
typedef INT8U			uchar;
typedef INT16U			uint;

typedef BYTE            byte;
typedef unsigned int    INT32U;

#ifndef NULL
	#define NULL (void *)0	   /*空指针*/
#endif

#define TRUE (1 == 1)
#define FALSE (1 == 0)

#define DEF_11059200

#endif