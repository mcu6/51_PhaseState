#define _LCD_SHOW_A_C_
#include "lcd_show_a.h"

/*模块结构体初始化*/
void lcd_show_a_struct_init(void)
{
	INT8U i;
	lcd_show_data.status = LCD_SHOW_STATUS_TIM_WAIT;
	for(i = 0;i < LCD_SHOW_TXT_NUM;i ++)
	{
		lcd_show_data.txt_buf[i] = '\0';
	}
	lcd_show_data.txt_buf[LCD_SHOW_TXT_NUM-1] = 0;
}

/*模块初始化*/
void lcd_show_a_init(LCD_SHOW_FLOAT *f_lcd_show_float,LCD_SHOW_STRING *f_lcd_show_string)
{
	const char str[]="Vol:      V";
	LCD_TXT_MSG1 msg_txt;

	lcd_show_a_struct_init();
	lcd_show_data.f_lcd_show_float = f_lcd_show_float;
	lcd_show_data.f_lcd_show_string = f_lcd_show_string;

	msg_txt.rows = 0;
	msg_txt.column = 0;
	msg_txt.txt = str;
	lcd_show_data.f_lcd_show_string((void *)(&msg_txt));
}

/*模块定时器中断函数*/
void lcd_show_a_tick(void)
{
   lcd_show_data.status = LCD_SHOW_STATUS_TIM_UP;
}



/*模块执行处理主函数*/
void lcd_show_a_exc(void)
{
	LCD_FLOAT_MSG1 msg1;
	LCD_TXT_MSG1 msg_txt;
	if(lcd_show_data.status == LCD_SHOW_STATUS_TIM_UP)
	{
		lcd_show_data.status = LCD_SHOW_STATUS_TIM_WAIT;
		msg1.rows = 0;
		msg1.column = 5;
		msg1.val = lcd_show_data.vol;
		lcd_show_data.f_lcd_show_float((void *)(&msg1));
		msg_txt.rows = 1;
		msg_txt.column = 0;
		msg_txt.txt = lcd_show_data.txt_buf;
		lcd_show_data.f_lcd_show_string((void *)(&msg_txt));
	}
}

void lcd_show_load_vol(float vol)
{
	lcd_show_data.vol = vol;	
}

void lcd_show_load_txt(char *str,INT8U num)
{
	INT8U i;
	if(num < (LCD_SHOW_TXT_NUM))
	{
		for(i = 0;i < num;i ++)
		{
			lcd_show_data.txt_buf[i] = *str++;	
		}
		lcd_show_data.txt_buf[i] = '\0';
	}
}


