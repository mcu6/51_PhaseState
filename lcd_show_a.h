/*
***********************************************************************************
*			
*功能：
*
*
*
*说明：1.
*      2.
*      3.
*
*
*By		:Jiang
*Contact	:1247612904@qq.com
*History	:2014/8/6 22:12:19
***********************************************************************************
*/
#ifndef _LCD_SHOW_A_H_
#define _LCD_SHOW_A_H_

#include "lb_type.h"
//#include "lcd_1602_odev.h"

#ifdef _LCD_SHOW_A_C_
#define LCD_SHOW_A_EXT
#else 
#define LCD_SHOW_A_EXT extern
#endif 

#ifdef _LCD_SHOW_A_C_
#endif 

#define LCD_SHOW_STATUS_TIM_UP		1
#define LCD_SHOW_STATUS_TIM_WAIT	0

typedef void *LCD_SHOW_FLOAT(void *msg);
typedef void *LCD_SHOW_STRING(void *str);

#define LCD_SHOW_TXT_NUM	4

typedef struct{
	INT8U rows;
	INT8U column;
	float val;
}LCD_FLOAT_MSG1;

typedef struct{
	INT8U rows;
	INT8U column;
	char *txt;
}LCD_TXT_MSG1;

typedef struct
{
    INT8U status;
	float vol;
	char txt_buf[LCD_SHOW_TXT_NUM];
	LCD_SHOW_FLOAT *f_lcd_show_float;
	LCD_SHOW_STRING *f_lcd_show_string;
}lcd_show_struct;

LCD_SHOW_A_EXT lcd_show_struct lcd_show_data;

LCD_SHOW_A_EXT void lcd_show_a_init(LCD_SHOW_FLOAT *f_lcd_show_float,LCD_SHOW_STRING *f_lcd_show_string);
LCD_SHOW_A_EXT void lcd_show_a_tick(void);
LCD_SHOW_A_EXT void lcd_show_a_exc(void);

LCD_SHOW_A_EXT void lcd_show_load_vol(float vol);
LCD_SHOW_A_EXT void lcd_show_load_txt(char *str,INT8U num);

#endif