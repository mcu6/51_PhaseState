/*
***********************************************************************************
*			
*功能：
*
*
*
*说明：1.
*      2.
*      3.
*
*
*By		:Jiang
*Contact	:1247612904@qq.com
*History	:2014/8/31 13:54:06
***********************************************************************************
*/
#ifndef _COMM_REC_H_
#define _COMM_REC_H_

#include "lb_type.h"
#include "timer_core.h"

#ifdef _COMM_REC_C_
#define COMM_REC_EXT
#else 
#define COMM_REC_EXT extern
#endif 

#define COMM_REC_NUM	3
#define COMM_REC_BUF_NUM	50

typedef INT8U FUNC_COMM_REC(void);
typedef INT8U FUNC_COMM_REC_OK(void *msg);

typedef struct
{
    INT8U status;
	INT8U addr;
	INT8U *buf;
	INT8U max_num;
	INT8U index;
	TIMER_STRUCT *tmr_span;
	FUNC_COMM_REC *f_comm_rec;
	FUNC_COMM_REC_OK *f_call_back;
	FUNC_COMM_REC_OK *f_call_back_int;
}comm_rec_struct;

#ifdef _COMM_REC_C_
comm_rec_struct comm_rec_data[COMM_REC_NUM];
INT8U comm_rec_alloc;

INT8U comm_rec_buf[COMM_REC_BUF_NUM];
INT8U buf_alloc;
#endif 

//COMM_REC_EXT 

COMM_REC_EXT void comm_rec_init(void);
COMM_REC_EXT comm_rec_struct *comm_rec_create(	INT8U addr,
												INT8U max_num,
												FUNC_COMM_REC *f_comm_rec,
												FUNC_COMM_REC_OK *f_call_back,
												FUNC_COMM_REC_OK *f_call_back_int);
COMM_REC_EXT void comm_rec_handle(comm_rec_struct *p_data);
COMM_REC_EXT void comm_rec_set_addr(comm_rec_struct *p_data,INT8U addr);
#endif