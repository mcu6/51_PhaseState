#ifndef _TIMER_CORE_H_
#define _TIMER_CORE_H_

#include "lb_type.h"

#ifdef _TIMER_CORE_C_
#define TIMER_CORE_EXT
#else
#define TIMER_CORE_EXT extern
#endif

#define TIMER_NUM	5

typedef void *FUNC_TIMER(void *msg); 

typedef struct{
	INT16U cnt;
	INT8U flag;
	INT16U interval;
	INT8U enable;
	FUNC_TIMER *f_callback;
	FUNC_TIMER *f_callback_int;
	void *msg;
}TIMER_STRUCT;

#ifdef _TIMER_CORE_C_
TIMER_STRUCT timer1[TIMER_NUM];
INT8U timer_alloc = 0;
#endif

TIMER_CORE_EXT TIMER_STRUCT *LB_TMRCreate(INT16U interval,INT8U enable,FUNC_TIMER *f_callback,FUNC_TIMER *f_callback_int,void *msg);
TIMER_CORE_EXT void InitLB_TMR(void);
TIMER_CORE_EXT void LB_ExcTMR(void);
TIMER_CORE_EXT void LB_TMRTick(void);
TIMER_CORE_EXT void Timer_Enalbe(TIMER_STRUCT *timer,INT16U enable);
TIMER_CORE_EXT void DisLB_TMR(TIMER_STRUCT *timer);
TIMER_CORE_EXT void EnLB_TMR(TIMER_STRUCT *timer);
TIMER_CORE_EXT void ReCntLB_TMR(TIMER_STRUCT *timer);
TIMER_CORE_EXT void SetLB_TMR(INT16U interval,TIMER_STRUCT *timer);
TIMER_CORE_EXT INT8U GetTimerNum(void);


#endif

