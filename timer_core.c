#define _TIMER_CORE_C_
#include "timer_core.h"

void InitLB_TMR(void)
{
	INT8U i;
	for(i = 0;i < TIMER_NUM;i ++)
	{
		timer1[i].flag = FALSE;
		timer1[i].cnt = 0;
		timer1[i].interval = 0;
		timer1[i].enable = FALSE;
	}
	
}

TIMER_STRUCT *LB_TMRCreate(INT16U interval,INT8U enable,FUNC_TIMER *f_callback,FUNC_TIMER *f_callback_int,void *msg)
{
	if(timer_alloc >= TIMER_NUM)
	{
		return NULL;
	}
	timer1[timer_alloc].enable = enable;
	timer1[timer_alloc].interval = interval;
	timer1[timer_alloc].f_callback = f_callback;
	timer1[timer_alloc].f_callback_int = f_callback_int;
	timer1[timer_alloc].msg = msg;
	timer_alloc ++;
	return &timer1[timer_alloc - 1];	
}

/*
*/
void DisLB_TMR(TIMER_STRUCT *timer)
{
	timer->enable = FALSE;
}

void EnLB_TMR(TIMER_STRUCT *timer)
{
	timer->enable = TRUE;	
}

void ReCntLB_TMR(TIMER_STRUCT *timer)
{
	 timer->cnt = 0;
}

void SetLB_TMR(INT16U interval,TIMER_STRUCT *timer)
{
	timer->interval = interval;
}

void Timer_Enalbe(TIMER_STRUCT *timer,INT16U enable)
{
	timer->enable = enable;
}

INT8U GetTimerNum(void)
{
	return timer_alloc;
}

void LB_ExcTMR(void)
{
	INT8U i;
	for(i = 0;i < TIMER_NUM;i ++)
	{
		if(timer1[i].flag == TRUE)
		{
			timer1[i].flag = FALSE;
			if(timer1[i].f_callback != NULL)
			{
				timer1[i].f_callback(timer1[i].msg);
			}
		}
	}	
}

void LB_TMRTick(void)
{
	INT8U i;
	for(i = 0;i < TIMER_NUM;i ++)
	{
		if(timer1[i].enable == TRUE)
		{
			timer1[i].cnt ++;
			if(timer1[i].cnt >= timer1[i].interval)
			{
				timer1[i].cnt = 0;
				if(timer1[i].flag == FALSE)
				{
					timer1[i].flag = TRUE;
					if(timer1[i].f_callback_int != NULL) 	//<--bug
					{
						timer1[i].f_callback_int(timer1[i].msg);
					}
				}	
			}
		}
	}
}
