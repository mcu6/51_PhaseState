#define _MAIN_C_
#include "all.h"

#include "stdio.h"
#include "string.h"

void application_tick()
{

	
	phase_state_struct * ptr = phase_state_get_data();
	load_comm_light(0,	ptr->red_light_state.enable,	
	                    ptr->red_light_state.startTime,	
	                    ptr->red_light_state.likelyEndTime);
	load_comm_light(1,	ptr->yellow_light_state.enable,	
	                    ptr->yellow_light_state.startTime,	
	                    ptr->yellow_light_state.likelyEndTime);
	load_comm_light(2,	ptr->green_light_state.enable,	
	                    ptr->green_light_state.startTime,	
	                    ptr->green_light_state.likelyEndTime);
	
	
//   lcd_show_load_vol(get_vol_smp());
//   lcd_show_load_txt(get_text(),3);
//   load_comm_vol(get_vol_smp());
//   load_comm_addr(get_text_addr());
}

/*模块结构体初始化*/
void main_struct_init(void)
{

}

/*模块定时器中断函数*/
void main_tick(void *msg)
{
   msg = msg;
//   lcd_show_a_tick();
//   mkey_odev_a_tick();
   communication_a_tick();
//   vol_smp_a_tick();
   application_tick();

   //lcd_show_load_txt(get_text(),3);

   //lcd_show_load_vol(get_vol_smp());
   //lcd_1602_wr_float(1,8,(float)get_text_addr());
   
}

void phase_state_tick(void *msg)
{
	 //char log_buf[30] = {0};
	 msg =msg;
	
	 //sprintf(log_buf,"[ %d : %d : %d ] \n", phase_state_data.red_light_state.light_cnt_surplus,\
																			phase_state_data.yellow_light_state.light_cnt_surplus,\
																			phase_state_data.green_light_state.light_cnt_surplus);
	  //exc_comm_reply(log_buf,strlen(log_buf));

   if (phase_state_data.red_light_state.enable == TRUE)

   {
      phase_state_data.red_light_state.light_cnt_surplus --;

      if (phase_state_data.red_light_state.light_cnt_surplus == 0)
      {
         phase_state_data.red_light_state.enable = FALSE;
         phase_state_data.yellow_light_state.enable = TRUE;

         phase_state_data.red_light_state.light_cnt_surplus = \
         phase_state_data.red_light_state.light_cnt_set ;
      }
   }

   if (phase_state_data.yellow_light_state.enable == TRUE)
   {
      phase_state_data.yellow_light_state.light_cnt_surplus --;

      if (phase_state_data.yellow_light_state.light_cnt_surplus ==0 )
      {
         phase_state_data.yellow_light_state.enable = FALSE;
         phase_state_data.green_light_state.enable = TRUE;

         phase_state_data.yellow_light_state.light_cnt_surplus = \
         phase_state_data.yellow_light_state.light_cnt_set ;
      }
   }

   if (phase_state_data.green_light_state.enable == TRUE)
   {
      phase_state_data.green_light_state.light_cnt_surplus --;

      if (phase_state_data.green_light_state.light_cnt_surplus ==0 )
      {
         phase_state_data.green_light_state.enable = FALSE;
         phase_state_data.red_light_state.enable = TRUE;
         phase_state_data.green_light_state.light_cnt_surplus = \
         phase_state_data.green_light_state.light_cnt_set ;
      }
   }



   if (phase_state_data.red_light_state.enable == TRUE){
      LIGHT_RED = FALSE;
      LIGHT_YELLOW =TRUE;
      LIGHT_GREEN =TRUE;
   }
   if (phase_state_data.yellow_light_state.enable == TRUE)
   {
      LIGHT_RED = TRUE;
      LIGHT_YELLOW =FALSE;
      LIGHT_GREEN =TRUE;

   }

   if (phase_state_data.green_light_state.enable == TRUE)
   {
      LIGHT_RED = TRUE;
      LIGHT_YELLOW =TRUE;
      LIGHT_GREEN =FALSE;

   }
}



void key_tick(INT8U key)
{
	key = key;
}

/*模块初始化*/
void main_init(void)
{
   InitLB_TMR();
   main_struct_init();
   timer0_idev_init();
   uart_idev_init();
   comm_rec_init();
   communication_a_init();
   comm_reply_init((f_comm_send_byte *)uart_send_byte);
//   lcd_1602_odev_init();
//   lcd_show_a_init((LCD_SHOW_FLOAT *)lcd_show_f,(LCD_SHOW_STRING *)lcd_show_s);
//   mkey_odev_a_init((F_MKEY_MSG	*)key_tick);
//   text_init();
////   adc_0832_odev_init();
//   vol_smp_a_init((FUNC_GET_AD_RESULT *)get_ad_result);
	
	 phase_state_odev_init();

   main_data.tmr_mtick = LB_TMRCreate(1,TRUE,(FUNC_TIMER *)main_tick,(FUNC_TIMER *)0,(void *)0);
	 LB_TMRCreate(2,TRUE,(FUNC_TIMER *)phase_state_tick, NULL,(void *)0);


   EA = 1;

   attach_key((F_MKEY_MSG	*)txt_key_tick);

}



/*模块执行处理主函数*/
void main_exc(void)
{
	LB_ExcTMR();
//	lcd_show_a_exc();
	// mkey_odev_a_exc();
	// vol_smp_a_exc();
	communication_a_exc();
}

/*主函数*/
void main(void)
{
    main_init();
    while(1)
    {
        main_exc();
    }
}

