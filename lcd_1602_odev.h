/*
***********************************************************************************
*			
*功能：
*
*
*
*说明：1.
*      2.
*      3.
*
*
*By		:Jiang
*Contact	:1247612904@qq.com
*History	:2014/8/3 9:12:11
***********************************************************************************
*/
#ifndef _LCD_1602_ODEV_H_
#define _LCD_1602_ODEV_H_

#include "lb_type.h"
#include "lcd_1602_odev_io.h"

#ifdef _LCD_1602_ODEV_C_
#define LCD_1602_ODEV_EXT
#else 
#define LCD_1602_ODEV_EXT extern
#endif 

#ifdef _LCD_1602_ODEV_C_
#endif 

typedef struct
{
    INT8U status;
}lcd_1602_struct;

LCD_1602_ODEV_EXT lcd_1602_struct lcd_1602_data;

LCD_1602_ODEV_EXT void lcd_1602_odev_init(void);
LCD_1602_ODEV_EXT void lcd_1602_wr_float(INT8U rows,INT8U column,float val);
LCD_1602_ODEV_EXT void lcd_1602_wr_string(INT8U rows,INT8U column,char *str); 
LCD_1602_ODEV_EXT void lcd_show_f(void *msg);
LCD_1602_ODEV_EXT void lcd_show_s(void *msg);
#endif