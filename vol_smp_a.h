/*
***********************************************************************************
*			
*功能：
*
*
*
*说明：1.
*      2.
*      3.
*
*
*By		:Jiang
*Contact	:1247612904@qq.com
*History	:2014/8/6 22:00:17
***********************************************************************************
*/
#ifndef _VOL_SMP_A_H_
#define _VOL_SMP_A_H_

#include "lb_type.h"
//#include "adc_0832_odev.h"

#ifdef _VOL_SMP_A_C_
#define VOL_SMP_A_EXT
#else 
#define VOL_SMP_A_EXT extern
#endif 

#ifdef _VOL_SMP_A_C_
#endif 

#define VOL_SMP_STATUS_TIM_UP	1
#define VOL_SMP_STATUS_TIM_WAIT	0

typedef INT8U FUNC_GET_AD_RESULT(void);

typedef struct
{
    INT8U status;
	FUNC_GET_AD_RESULT *f_get_ad_result;
	float voltage;
}vol_smp_struct;

VOL_SMP_A_EXT vol_smp_struct vol_smp_data;

VOL_SMP_A_EXT void vol_smp_a_init(FUNC_GET_AD_RESULT *f_get_ad_result);
VOL_SMP_A_EXT void vol_smp_a_tick(void);
VOL_SMP_A_EXT void vol_smp_a_exc(void);
VOL_SMP_A_EXT float get_vol_smp(void);

#endif