/*
***********************************************************************************
*			
*功能：
*
*
*
*说明：1.
*      2.
*      3.
*
*
*By		:Jiang
*Contact	:1247612904@qq.com
*History	:2014/9/7 10:59:51
***********************************************************************************
*/
#ifndef _COMM_REPLY_H_
#define _COMM_REPLY_H_

#include "lb_type.h"

#ifdef _COMM_REPLY_C_
#define COMM_REPLY_EXT
#else 
#define COMM_REPLY_EXT extern
#endif 

#ifdef _COMM_REPLY_C_
#endif

typedef void *f_comm_send_byte(INT8U dat); 

typedef struct
{
    INT8U status;
	f_comm_send_byte *f_send_byte;
}comm_reply_struct;

COMM_REPLY_EXT comm_reply_struct comm_reply_data;

COMM_REPLY_EXT void comm_reply_init(f_comm_send_byte *f_send_byte);
COMM_REPLY_EXT void exc_comm_reply(INT8U *buf,INT8U num);
#endif