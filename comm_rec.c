#define _COMM_REC_C_
#include "comm_rec.h"

/*模块结构体初始化*/
void comm_rec_struct_init(void)
{
	 comm_rec_alloc = 0;
	 buf_alloc = 0;
} 

void comm_rec_ok(void *msg)
{
	comm_rec_struct *p_data;
	p_data = (comm_rec_struct *)msg;

	DisLB_TMR(p_data->tmr_span);

	if(p_data->f_call_back != NULL)
	{
		if(p_data->addr == p_data->buf[0])
		{
			p_data->f_call_back((void *)p_data);
		}
	}

	p_data->index = 0;
}

void comm_rec_ok_int(void *msg)
{
	comm_rec_struct *p_data;
	p_data = (comm_rec_struct *)msg;

	DisLB_TMR(p_data->tmr_span);

	if(p_data->f_call_back_int != NULL)
	{
		if(p_data->addr == p_data->buf[0])
		{
			p_data->f_call_back_int((void *)0);
		}
	}	
}

void comm_rec_handle(comm_rec_struct *p_data)
{
	EnLB_TMR(p_data->tmr_span);
	ReCntLB_TMR(p_data->tmr_span);
	p_data->buf[p_data->index] = p_data->f_comm_rec();
	p_data->index ++;
	if(p_data->index == p_data->max_num)	
	{
		p_data->index	= p_data->max_num - 1;
	}
}

/*模块初始化*/
void comm_rec_init()
{

   comm_rec_struct_init();


}

void comm_rec_set_addr(comm_rec_struct *p_data,INT8U addr)
{
	p_data->addr = addr;
}

comm_rec_struct *comm_rec_create(	INT8U addr,
									INT8U max_num,
									FUNC_COMM_REC *f_comm_rec,
									FUNC_COMM_REC_OK *f_call_back,
									FUNC_COMM_REC_OK *f_call_back_int)
{
	comm_rec_struct *p_data;
	
	if(comm_rec_alloc >= COMM_REC_NUM)
	{
		return NULL;
	}
	p_data = &comm_rec_data[comm_rec_alloc];
	comm_rec_alloc ++;

	if((buf_alloc + max_num) <= COMM_REC_BUF_NUM)
	{
		p_data->buf = &comm_rec_buf[buf_alloc];
		buf_alloc += max_num;
		p_data->max_num = max_num;
	}
	else
	{
		return NULL;
	}
	p_data->addr = addr;
	p_data->f_comm_rec = f_comm_rec;
	p_data->f_call_back = f_call_back;
	p_data->f_call_back_int = f_call_back_int;
	
	p_data->tmr_span = LB_TMRCreate(2,FALSE,(FUNC_TIMER *)comm_rec_ok,(FUNC_TIMER *)comm_rec_ok_int,(void *)p_data);
	p_data->index = 0;
	return p_data; 	
}

